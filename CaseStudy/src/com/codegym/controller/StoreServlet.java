package com.codegym.controller;

import com.codegym.model.Store;
import com.codegym.service.StoreJDBCServiceImpl;
import com.codegym.service.StoreService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "StoreServlet", urlPatterns = "/store")
public class StoreServlet extends HttpServlet {
    private StoreService storeService = new StoreJDBCServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
            createStore(request,response);
            break;
            case "edit":
                updateStore(request,response);
            break;
            case "delete":
                deleteStore(request,response);
                break;
            default:
                listStore(request,response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                showCreateForm(request,response);
                break;
            case "edit":
                showEditForm(request,response);
                break;
            case "delete":
                showDeleteForm(request,response);
                break;
            case "view":
                viewStore(request,response);
            default:
                listStore(request,response);
                break;
        }
    }
    private void viewStore(HttpServletRequest request, HttpServletResponse response) {
        int idStore = Integer.parseInt(request.getParameter("idStore"));
        Store stores = this.storeService.findByIdStore(idStore);
        RequestDispatcher dispatcher;
        if(stores == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("stores", stores);
            dispatcher = request.getRequestDispatcher("Store/view.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void listStore(HttpServletRequest request, HttpServletResponse response) {
        List<Store> stores = this.storeService.findall();
        request.setAttribute("stores", stores);

        RequestDispatcher dispatcher = request.getRequestDispatcher("Store/list.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void createStore(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String PhoneNumber = request.getParameter("PhoneNumber");

        Store stores = new Store( name, address, PhoneNumber);
        this.storeService.save(stores);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Store/create.jsp");
        request.setAttribute("message", "New store was created");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("Store/create.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void updateStore(HttpServletRequest request, HttpServletResponse response) {
        int idStore = Integer.parseInt(request.getParameter("idStore"));
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String PhoneNumber = request.getParameter("PhoneNumber");
        Store  stores = storeService.findByIdStore(idStore);
        RequestDispatcher dispatcher;
        if(stores == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            stores.setName(name);
            stores.setPhoneNumber(PhoneNumber);
            stores.setAddress(address);
            this.storeService.update(idStore, stores);
            request.setAttribute("stores", stores);
            request.setAttribute("message", "Store information was updated");
            dispatcher = request.getRequestDispatcher("Store/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        int idStore = Integer.parseInt(request.getParameter("idStore"));
        Store stores= this.storeService.findByIdStore(idStore);
        RequestDispatcher dispatcher;
        if(stores == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("stores", stores);
            dispatcher = request.getRequestDispatcher("Store/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void deleteStore(HttpServletRequest request, HttpServletResponse response) {
        int idStore = Integer.parseInt(request.getParameter("idStore"));
        Store stores= this.storeService.findByIdStore(idStore);
        RequestDispatcher dispatcher;
        if(stores == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.storeService.remove(idStore);
            try {
                response.sendRedirect("/store");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        int idStore = Integer.parseInt(request.getParameter("idStore"));
        Store stores= this.storeService.findByIdStore(idStore);
        RequestDispatcher dispatcher;
        if(stores == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("stores", stores);
            dispatcher = request.getRequestDispatcher("Store/delete.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

