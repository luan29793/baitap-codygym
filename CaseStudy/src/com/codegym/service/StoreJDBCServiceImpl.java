package com.codegym.service;

import com.codegym.model.Store;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StoreJDBCServiceImpl implements StoreService{
    private String jdbcURL = "jdbc:mysql://localhost:3306/inventory_management";
    private String jdbcUsername = "root";
    private String jdbcPassword = "Lluan_1993";
    private static final String INSERT_USERS_SQL = "INSERT INTO store" + " ( name, address,phoneNumber) VALUES "
            + " (?, ?, ?);";
    private static final String UPDATE_USER_SQL_ISDELETE = "UPDATE store set isDelete ='1',deleteBy = ? WHERE idStore = ?;";
    private static final String SELECT_USER_BY_ID = "select idStore,name,address,PhoneNumber from store where idStore =?";
    private static final String SELECT_ALL_USERS = "select idStore,name, address, phoneNumber from store";
    private static final String DELETE_USERS_SQL = "delete from store where idStore = ?;";
    private static final String UPDATE_USERS_SQL = "update store set name = ?, address = ?, phoneNumber = ? where idStore = ?;";


    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<Store> findall() {
        List<Store> storeslist = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int idStore = rs.getInt("idStore");
                String name = rs.getString("name");
                String address = rs.getString("address");
                String PhoneNumber = rs.getString("PhoneNumber");
                storeslist.add(new Store(idStore, name,address,PhoneNumber));
            }

        } catch (SQLException e) {
            printSQLException(e);
        }
        return storeslist;
    }

    @Override
    public Store findByIdStore(int idStore) {
            Store store = null;
            try (Connection connection = getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
                preparedStatement.setInt(1, idStore);
                System.out.println(preparedStatement);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    String name = rs.getString("name");
                    String address = rs.getString("address");
                    String PhoneNumber = rs.getString("PhoneNumber");
                    store = new Store(idStore, name,address, PhoneNumber);
                }
            } catch (SQLException e) {
                printSQLException(e);
            }
            return store;
        }



    @Override
    public void save(Store store) {
        System.out.println(INSERT_USERS_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {
            preparedStatement.setString(1, store.getName());
            preparedStatement.setString(2, store.getAddress());
            preparedStatement.setString(3, store.getPhoneNumber());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }


    @Override
    public void update(int idStore, Store store) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);) {
            statement.setString(1, store.getName());
            statement.setString(2, store.getAddress());
            statement.setString(3, store.getPhoneNumber());
            statement.setInt(4, store.getIdStore());
            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }

    }

    @Override
    public void remove(int idStore) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USER_SQL_ISDELETE);)
        {
            statement.setInt(1, idStore);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
