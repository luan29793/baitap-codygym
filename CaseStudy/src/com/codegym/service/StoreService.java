package com.codegym.service;

import com.codegym.model.Store;
import java.util.List;;

public interface StoreService {
    List<Store> findall();
    Store findByIdStore(int IdStore);
    void save (Store store);
    void update (int idStore, Store store);
    void remove (int idStore);
}
