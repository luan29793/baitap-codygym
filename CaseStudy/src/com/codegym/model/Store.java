package com.codegym.model;

import java.util.Date;

public class Store {
    private int idStore;
    private String name;
    private String address;
    private String PhoneNumber;
    private int isDelete;
    private String DeleteBy;
    private Date DeleteDate;
    private String ModifyBy;
    private String MobifyDate;

    public Store() {
    }
    public Store(int idStore, int isDelete, String DeleteBy) {
        this.idStore =idStore;
        this.isDelete = isDelete;
        this.DeleteBy = DeleteBy;
    }
    public Store(int idStore,String name) {
        this.idStore= idStore;
        this.name =name;
    }

    public Store(int idStore, String name, String address, String PhoneNumber) {
        this.idStore = idStore;
        this.name = name;
        this.address = address;
        this.PhoneNumber = PhoneNumber;
    }
    public Store( String name, String address, String PhoneNumber) {
        this.name = name;
        this.address = address;
        this.PhoneNumber = PhoneNumber;
    }



    public int getIdStore() {
        return idStore;
    }

    public void setIdStore(int idStore) {
        this.idStore = idStore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getDeleteBy() {
        return DeleteBy;
    }

    public void setDeleteBy(String deleteBy) {
        DeleteBy = deleteBy;
    }

    public Date getDeleteDate() {
        return DeleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        DeleteDate = deleteDate;
    }

    public String getModifyBy() {
        return ModifyBy;
    }

    public void setModifyBy(String modifyBy) {
        ModifyBy = modifyBy;
    }

    public String getMobifyDate() {
        return MobifyDate;
    }

    public void setMobifyDate(String mobifyDate) {
        MobifyDate = mobifyDate;
    }
}

