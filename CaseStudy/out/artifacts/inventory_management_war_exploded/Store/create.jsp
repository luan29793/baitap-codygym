<%--
  Created by IntelliJ IDEA.
  User: luan1993
  Date: 11/10/19
  Time: 6:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Store</title>
    <style>
        .message {
            color:green;
        }
    </style>
</head>
<body>
<h1>Create Store</h1>
<p> <c:if test='${requestScope["message"] != null}'>
    <span class="message">${requestScope["message"]}</span>
</c:if>
</p>
<p>
    <a href="/store">Back to Store</a>
</p>
    <form method="post">
        <fieldset>
            <legend>Store information</legend>
            <table>
                <tr>
                    <td> Name Store:</td>
                    <td><input type="text" name="name" id = "name"> </td>
                    <td> Address Store:</td>
                    <td><input type="text" name="address" id = "address"> </td>
                    <td> PhoneNumber:</td>
                    <td><input type="text" name="PhoneNumber" id = "PhoneNumber"> </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Create Store" ></td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
