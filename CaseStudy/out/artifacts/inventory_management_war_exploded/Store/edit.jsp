<%--
  Created by IntelliJ IDEA.
  User: luan1993
  Date: 11/10/19
  Time: 6:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Store</title>
</head>
<body>
<h1>Edit Store</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/store">Back to Store list</a>
</p>
<form method="post">
    <fieldset>
        <legend>Store information</legend>
        <table>
            <tr>
                <td>Name Store: </td>
                <td><input type="text" name="name" id="name" value="${requestScope["stores"].getName()}"></td>
            </tr>
            <tr>
                <td>Address Store: </td>
                <td><input type="text" name="address" id="address" value="${requestScope["stores"].getAddress()}"></td>
            </tr>
            <tr>
                <td>PhoneNumber Store </td>
                <td><input type="text" name="PhoneNumber" id="PhoneNumber" value="${requestScope["stores"].getPhoneNumber()}"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Update Store"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
