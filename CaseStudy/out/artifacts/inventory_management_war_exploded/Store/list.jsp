<%--
  Created by IntelliJ IDEA.
  User: luan1993
  Date: 11/10/19
  Time: 6:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Store list</title>
</head>
<body>
    <h1>Store list</h1>
<p><a href="/store?action=create">Create new store</a> </p>
<table border="1">
    <tr>
        <td>id Store</td>
        <td>Name</td>
        <td>Address</td>
        <td>Phone Number</td>
        <td>Edit</td>
        <td>Delete</td>

    </tr>
    <c:forEach items='${requestScope["stores"]}' var="store">
    <tr>
        <td>${store.getIdStore()}</td>
        <td><a href="/store?action=view&idStore=${store.getIdStore()}">${store.getName()}</a></td>
        <td>${store.getAddress()}</td>
        <td>${store.getPhoneNumber()}</td>
        <td><a href="/store?action=edit&idStore=${store.getIdStore()}">edit</a></td>
        <td><a href="/store?action=delete&idStore=${store.getIdStore()}">delete</a></td>
        </tr>
    </c:forEach>
    </table>

</body>
</html>
