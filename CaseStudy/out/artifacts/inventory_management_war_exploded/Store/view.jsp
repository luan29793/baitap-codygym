<%--
  Created by IntelliJ IDEA.
  User: luan1993
  Date: 11/11/19
  Time: 10:53 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Store</title>
</head>
<body>
<h1>Store details</h1>
<p>
    <a href="/store">Back to store list</a>
</p>
<table>
    <tr>
        <td>Name Store: </td>
        <td>${requestScope["stores"].getName()}</td>
    </tr>

    <tr>
        <td>Address: </td>
        <td>${requestScope["stores"].getAddress()}</td>
    </tr>
    <tr>
        <td>PhoneNumber: </td>
        <td>${requestScope["stores"].getPhoneNumber()}</td>
    </tr>
</table>
</body>
</html>
